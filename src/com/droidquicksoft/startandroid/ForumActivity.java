package com.droidquicksoft.startandroid;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class ForumActivity extends ActionBarActivity {

    private WebView wv;
    Menu myMenu = null;
    private static final String PREFS_NAME = "MyPrefs";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.forum);

        wv = (WebView) findViewById(R.id.webView_forum1);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        WebSettings webSettings = wv.getSettings();
        webSettings.setSavePassword(true);
        webSettings.setSaveFormData(true);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(true);
        webSettings.setUseWideViewPort(true);

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        webSettings.setLoadsImagesAutomatically(true);
        wv.getSettings().setBuiltInZoomControls(true);

        wv.setWebViewClient(new WebViewClient() {

            public void onLoadResource(WebView view, String url) {
                view.loadUrl("javascript:(function() { " +
                        "remove('logodesc');" +
                        "remove('wrapfooter');" +
                        "function remove(id){if (document.getElementById(id)){document.getElementById(id).style['display'] = 'none';}}" +
                        "})()");
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(), "Ошибка: " + description + " " + failingUrl, Toast.LENGTH_LONG).show();
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.indexOf("startandroid") <= 0) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                    return true;
                }
                return false;
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                actionBar.setTitle("Идет загрузка...");

                view.getSettings().setLoadsImagesAutomatically(false);
                view.getSettings().setSupportZoom(true);
            }


            public void onPageFinished(WebView view, String url) {
                actionBar.setTitle("Форум");
                wv.setVisibility(0);
                view.getSettings().setLoadsImagesAutomatically(true);
                view.getSettings().setSupportZoom(true);
            }
        });
        wv.loadUrl("http://forum.startandroid.ru");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        this.myMenu = menu;
        MenuItem item2 = menu.add(0, 2, 0, "Назад");
        MenuItem item3 = menu.add(0, 3, 0, "Обновить");
        MenuItem item4 = menu.add(0, 4, 0, "Очистить кэш");
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && wv.canGoBack()) {
            wv.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2:
                wv.goBack();
                break;
            case 3:
                wv.reload();
                break;
            case 4:
                wv.clearCache(true);
                break;
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, StartActivity.class);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.from(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
        }

        return true;
    }


    protected void OnDestroy() {
        super.onDestroy();
        wv.destroy();
    }

}
