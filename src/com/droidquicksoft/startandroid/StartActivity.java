package com.droidquicksoft.startandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class StartActivity extends ActionBarActivity implements OnClickListener {
    private static final int NUM_ITEMS = 2;
    private static final String TAG = "myLogs";
    PagerAdapter mAdapter;
    ViewPager mPager;
    Button BtnForum;
    Button BtnLessons;
    Button BtnNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "created");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);

        BtnLessons = (Button) findViewById(R.id.BtnLessons);
        BtnLessons.setOnClickListener(this);

        BtnNews = (Button) findViewById(R.id.BtnNews);
        BtnNews.setOnClickListener(this);

        BtnForum = (Button) findViewById(R.id.BtnForum);
        BtnForum.setOnClickListener(this);

        mAdapter = new PagerAdapter(getSupportFragmentManager());

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

    }

    public static class PagerAdapter extends FragmentStatePagerAdapter {
        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return StartFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }
    }

    public static class StartFragment extends Fragment {
        private int mNum;

        static StartFragment newInstance(int num) {
            StartFragment sf = new StartFragment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("num", num);
            sf.setArguments(args);

            return sf;
        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Bundle args = getArguments();
            mNum = args != null ? args.getInt("num") : 1;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Set up the various layouts on different flip pages.
            final int[] layouts = {
                    R.layout.faq,
                    R.layout.about,
                    /*R.layout.advertisement,*/
                    /*R.layout.license,*/};
            int layoutId = layouts[mNum];
            View v = inflater.inflate(layoutId, container, false);
            return v;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;

        switch (v.getId()) {
            case R.id.BtnLessons:
                intent = new Intent(this, LessonsActivity.class);
                startActivity(intent);
                break;
            case R.id.BtnNews:
                intent = new Intent(this, NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.BtnForum:
                intent = new Intent(this, ForumActivity.class);
                startActivity(intent);
                break;
        }

    }

}
